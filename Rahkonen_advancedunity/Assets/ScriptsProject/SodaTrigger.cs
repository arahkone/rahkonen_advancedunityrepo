﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SodaTrigger : MonoBehaviour
{
    public AudioClip soda;

    public AudioSource source;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("player"))
        {
            source.PlayOneShot(soda);
        }
    }

}
