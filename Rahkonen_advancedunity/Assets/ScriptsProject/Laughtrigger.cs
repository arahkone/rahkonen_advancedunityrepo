﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Laughtrigger : MonoBehaviour
{

    public AudioClip laugh;

    public AudioSource source;
    private float lowVolumeRange = .25f;
    private float highVolumeRange = 1.25F;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        source.volume = Random.Range(lowVolumeRange, highVolumeRange);
        if (other.gameObject.CompareTag("player"))
        {
            source.PlayOneShot(laugh);
        }
    }
}
