﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class Theaterscript : MonoBehaviour
{


    public AudioMixer mixer;
    public AudioMixerSnapshot[] snapshots;
    public float[] weights;
    public float transitiontime;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("player"))
        {
            print("entered");
            weights[0] = 0f;
            weights[1] = 1f;
            mixer.TransitionToSnapshots(snapshots, weights, transitiontime);
        }
      
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.CompareTag("player"))
        {
            print("exited");
            weights[0] = 1f;
            weights[1] = 0f;
            mixer.TransitionToSnapshots(snapshots, weights, transitiontime);
        }

    }



}
