﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Horntrigger : MonoBehaviour
{
    public AudioClip horn;

    public AudioSource source;
    private float lowPitchRange = .75f;
    private float highPitchRange = 1.25F;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        source.pitch = Random.Range(lowPitchRange, highPitchRange);
        if (other.gameObject.CompareTag("player"))
        {
            source.PlayOneShot(horn);
        }
    }
}
